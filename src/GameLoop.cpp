#include "GameLoop.h"

int GameLoop::difficulty_ = 1;

GameLoop::GameLoop()
{
	startGame_ = true;
	level_ = NULL;
}

GameLoop::~GameLoop()
{
	if (level_ != NULL)
		delete level_;
}

void GameLoop::Initiate()
{
    startGame_ = mainMenu_.display();

    while (startGame_){
        level_ = new Level();

        startGame_ = level_->start();
        delete level_;
    }
}

