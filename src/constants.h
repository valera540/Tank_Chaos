#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <SFML/Graphics.hpp>
#include <Box2D.h>
#include <iostream>

// Resoures folder path
const sf::String PATH = "/home/garvel/MEGA/Projects/SFMLCpp/TanksLinux/src/res/";

/* ######## Box2D constants ######## */
// For meters->pixels converting
const float TO_PIXELS  =  40.0f;

// For pixels->meters converting
const float TO_METERS  =  0.025f;

//For radians->degree converting
const float DEG	 =  57.29577f;

// Reverse force (gravity analog)
const float REV_FORCE = 7.0f;

// Inertion limiter
const float INERT_LIMITER = 5.0f;

// Bullet speed
const float BULLET_SPEED = 12.0f;
/* ################################## */


#endif // CONSTANTS_H
