#pragma once

#include "Entity.h"
#include "Bullet.h"
#include <list>
#include <Box2D.h>

struct HeroInfo
{
    String currHealth, maxHealth, damage, x, y, vel_x, vel_y;
};

class Hero : public Entity
{
public:
	

    Hero(float, float, b2World*);
	~Hero();

    void      attack(b2World&)        override;
    void      takeDamage(int)         override;
    void      display(RenderWindow&)  override;
    void      moving(b2World&)        override;
	HeroInfo& getHeroInfo();
    void      updateHeroInfo(const b2Vec2&);
    b2Vec2    getPosition();
    bool      getLifeStatus();
private:
	
    HeroInfo heroInfo_;
};
