#pragma once

#include "MainMenu.h"
#include "Level.h"

class GameLoop
{
public:
    static int difficulty_;

    GameLoop();
    ~GameLoop();

	void	Initiate();

private:	
	MainMenu	mainMenu_;
	Level*		level_;
	bool		startGame_;

};
