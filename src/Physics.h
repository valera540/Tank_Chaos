#ifndef PHYSICS_H
#define PHYSICS_H

#include "constants.h"
#include "Bullet.h"
#include "Entity.h"
#include "Enemy.h"

class Physics: public b2ContactListener
{
    void BeginContact(b2Contact*) override;
};

#endif // PHYSICS_H
