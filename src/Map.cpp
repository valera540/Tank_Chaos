﻿#include "Map.h"
#include <iostream>
#include <fstream>

#include "constants.h"

Map::Map(b2World* world)
{
    std::ifstream in(PATH + "map.txt", std::ios::in);
	if (!in)  
		std::cout << "|  ERROR   | Map | map.txt open error!" << '\n'; 
	
	std::string line;
	field_ = new std::string[HIGHT_MAP];
	
	for (int i = 0; i < HIGHT_MAP; i++) {
		std::getline(in, line);
		field_[i] = line;
	}

	in.close();

    mapTiles.loadFromFile(PATH + "map_tiles.png");

    buildPhysics(world);
}

Map::~Map()
{
	delete[] field_;
}

void Map::buildPhysics(b2World* world)
{
    String solid_walls = "|-()[]+T<>";

    for (int i = 0; i < HIGHT_MAP; i++)
        for (int j = 0; j < WIDTH_MAP; j++)
            if (solid_walls.find(field_[i][j]) != String::InvalidPos) {
                b2PolygonShape wall;
                wall.SetAsBox((64.0f/2.0f)  * TO_METERS, (64.0f/2.0f) * TO_METERS);

                b2BodyDef bodyDef;
                bodyDef.position.Set((j*64.0f + 32.0f)*TO_METERS, (i*64.0f + 32.0f)*TO_METERS);

                b2Body* body = world->CreateBody(&bodyDef);

                body->CreateFixture(&wall, 1);
            }
}

void Map::drawSprite(int i, int j, int tile_x, int tile_y, RenderWindow& window)
{
    Sprite sprite;
	sprite.setTexture(mapTiles);
	sprite.setTextureRect(IntRect(tile_x, tile_y, 64, 64));
    sprite.setPosition(j * 64.0f, i * 64.0f);
	window.draw(sprite);
}

void Map::display(RenderWindow& window)
{
	for (int i = 0; i < HIGHT_MAP; i++) 
		for (int j = 0; j < WIDTH_MAP; j++)
			switch (field_[i][j]) {
				case '.' :		// Grass
					drawSprite(i, j, 64, 0, window);
					break;
			
				case '|' :		// Vertical wall
					drawSprite(i, j, 320, 0, window);
					break;

				case '-' :		//Horizontal wall
					drawSprite(i, j, 256, 0, window);
					break;

                case '(' :		// Left top corner
					drawSprite(i, j, 128, 0, window);
					break;

                case ')' :		// Right top corner
					drawSprite(i, j, 192, 0, window);
					break;

                case '[' :		// Left bottom corner
					drawSprite(i, j, 128, 64, window);
					break;

                case ']' :		// Right bottom corner
					drawSprite(i, j, 192, 64, window);
					break;

                case '+' :		// Vetrical top ending wall
					drawSprite(i, j, 256, 64, window);
					break;

                case 'T' :		// Vertical bottom ending wall
					drawSprite(i, j, 320, 64, window);
					break;
			
				case '#' :		// Floor
					drawSprite(i, j, 64, 64, window);
					break;
			
                case '%' :		// Rock
                    drawSprite(i, j, 64, 0, window);
                    drawSprite(i, j, 0, 0, window);
					break;

                case '<' :      // Horizontal left ending wall
                    drawSprite(i, j, 384, 0, window);
                    break;

                case '>' :      // Horizontal right ending wall
                    drawSprite(i, j, 448, 0, window);
                    break;

				default : 
					std::cout << "| Warning! | Map | Unknown char" << '\n';
			}
	
}

