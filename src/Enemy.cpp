#include "Enemy.h"
#include "constants.h"
#include "Bullet.h"
#include "GameLoop.h"
#include <cstdlib>
#include <ctime>

using namespace sf;

bool Enemy::HaveDeadEnemy = false;

void Enemy::EnemiesFactory(b2World& world, std::list<Enemy*>& eList)
{
    eList.push_back(new Enemy(5.0f,  8.0f, world));
    eList.push_back(new Enemy(21.0f, 6.0f, world));
    eList.push_back(new Enemy(25.0f, 6.0f, world));
    eList.push_back(new Enemy(28.0f, 7.0f, world));
}

void Enemy::EnemiesDisplay(RenderWindow& window, std::list<Enemy*>& eList)
{
    if (HaveDeadEnemy) {
        for (auto itr = eList.begin(); itr != eList.end(); )
            if ((*itr)->iDead_) {
                delete *itr;
                eList.erase(itr++);
            }
            else
                itr++;
        HaveDeadEnemy = false;
    }

    for (auto itr = eList.begin(); itr != eList.end(); itr++) {
        window.draw((*itr)->entitySprite_);
    }
}

void Enemy::EnemiesMoving(b2World& world, std::list<Enemy*>& eList)
{
    for (auto itr = eList.begin(); itr != eList.end(); itr++) {
        (*itr)->moving(world);
    }
}

bool Enemy::getLifeStatus(std::list<Enemy*>& eList)
{
    if (eList.size()) return true;

    return false;
}

Enemy::Enemy(float x, float y, b2World& world)
{
    maxHealth_ = currentHealth_ = 5 * GameLoop::difficulty_;
    damage_ = 3;
    speed_ = 4.0f;
    nameId_ = "enemy";
    changeWayTime_ = seconds(0.4f);
    rateOfFire_ = seconds(1.8f);
    direction_ = 'N';
    iDead_ = false;
    changeWay_ = false;
    needSomeMove_ = false;
    x_ = x * 64.0f;
    y_ = y * 64.0f;

    entityTexture_ = new Texture();
    if (!entityTexture_->loadFromFile(PATH + "enemy.png"))
        std::cout << "|  ERROR   |ENEMY| enemy.png texture open error!" << '\n';

    /*###### PHYSICS BLOCK ######*/
    b2BodyDef bodyDef;
    bodyDef.position.Set(x_ * TO_METERS, y_ * TO_METERS);
    bodyDef.type = b2_dynamicBody;

    entityPhysicsBody_ = world.CreateBody(&bodyDef);

    b2PolygonShape shape;
    shape.SetAsBox(54.0f/2.0f * TO_METERS, 55.0f/2.0f * TO_METERS);

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.3f;

    entityPhysicsBody_->CreateFixture(&fixtureDef);
    entityPhysicsBody_->SetUserData(this);
    entityPhysicsBody_->SetFixedRotation(true);
    /*###########################*/

    entitySprite_.setTexture(*entityTexture_);
    entitySprite_.setPosition(entityPhysicsBody_->GetPosition().x*TO_PIXELS, entityPhysicsBody_->GetPosition().y*TO_PIXELS);
    entitySprite_.setOrigin(54.0f/2.0f, 55.0f/2.0f);

    x_ = entityPhysicsBody_->GetPosition().x * TO_PIXELS;
    y_ = entityPhysicsBody_->GetPosition().y * TO_PIXELS;
    entitySprite_.setPosition(x_, y_);

    entityPhysicsBody_->SetLinearVelocity(b2Vec2(0.0f, -speed_));
    entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 0.0f );
}

Enemy::~Enemy()
{
    delete entityTexture_;
    entityPhysicsBody_->GetWorld()->DestroyBody(entityPhysicsBody_);
}

void Enemy::attack(b2World& world)
{
    if (clockOfFire_.getElapsedTime() > rateOfFire_) {
        Bullet::ActiveBullets.push_back(new Bullet(b2Vec2(x_, y_), world, direction_, damage_, "enemy"));
        clockOfFire_.restart();
    }
}

void Enemy::setChangeWayTrue()
{
    changeWay_ = true;
    needSomeMove_ = true;
}

void Enemy::moving(b2World& world)
{
    attack(world);

    if (changeWay_) {
        if (needSomeMove_){
            changeWayClock_.restart();
            switch (direction_) {
                case 'N':
                    direction_ = 'S';
                    entityPhysicsBody_->SetLinearVelocity(b2Vec2(0.0f, speed_));
                    entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 180.0f / DEG);
                    break;
                case 'S':
                    direction_ = 'N';
                    entityPhysicsBody_->SetLinearVelocity(b2Vec2(0.0f, -speed_));
                    entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 0.0f / DEG);
                    break;
                case 'W':
                    direction_ = 'E';
                    entityPhysicsBody_->SetLinearVelocity(b2Vec2(speed_, 0.0f));
                    entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 90.0f / DEG);
                    break;
                case 'E':
                    direction_ = 'W';
                    entityPhysicsBody_->SetLinearVelocity(b2Vec2(-speed_, 0.0f));
                    entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 270.0f / DEG);
                    break;
            }
            entitySprite_.setRotation(entityPhysicsBody_->GetAngle() * DEG);
            needSomeMove_ = false;
        }
        if (changeWayClock_.getElapsedTime() > changeWayTime_) {
            srand(time(0));
            int dir;
            bool correctRand = false;
            while(!correctRand) {
                dir = rand() % 4 + 1;
                if (!((dir == 1 && direction_ == 'S') ||
                      (dir == 2 && direction_ == 'W') ||
                      (dir == 3 && direction_ == 'E') ||
                      (dir == 4 && direction_ == 'N') ))
                    correctRand = true;
            }
            switch(dir) {
                case 1: direction_ = 'N'; break;
                case 2: direction_ = 'E'; break;
                case 3: direction_ = 'W'; break;
                case 4: direction_ = 'S'; break;
            }
            changeWay_ = false;
        }
    }
    else {
        switch (direction_) {
            case 'N':
                entityPhysicsBody_->SetLinearVelocity(b2Vec2(0.0f, -speed_));
                entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 0.0f );
                break;
            case 'S':
                entityPhysicsBody_->SetLinearVelocity(b2Vec2(0.0f, speed_));
                entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 180.0f / DEG);
                break;
            case 'W':
                entityPhysicsBody_->SetLinearVelocity(b2Vec2(-speed_, 0.0f));
                entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 270.0f / DEG);
                break;
            case 'E':
                entityPhysicsBody_->SetLinearVelocity(b2Vec2(speed_, 0.0f));
                entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 90.0f / DEG);
                break;
        }
        entitySprite_.setRotation(entityPhysicsBody_->GetAngle() * DEG);
    }

    x_ = entityPhysicsBody_->GetPosition().x * TO_PIXELS;
    y_ = entityPhysicsBody_->GetPosition().y * TO_PIXELS;
    entitySprite_.setPosition(x_, y_);
}

void Enemy::display(RenderWindow& window)
{
    window.draw(entitySprite_);
}

void Enemy::takeDamage(int damage)
{
    currentHealth_ -= damage;

    if (currentHealth_ <= 0) {
        iDead_ = true;
        HaveDeadEnemy = true;
    }
}

