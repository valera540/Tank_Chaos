#pragma once

#include <SFML/Graphics.hpp>
#include <Box2D.h>
using namespace sf;

class Map
{
public:
    Map(b2World*);
    ~Map();

	void	display(RenderWindow&);

private:	
	std::string*		field_;
	Texture				mapTiles;

    static const int	HIGHT_MAP = 20;
	static const int	WIDTH_MAP = 32;
	
	void	drawSprite(int, int, int, int, RenderWindow&);
    void    buildPhysics(b2World*);

};
