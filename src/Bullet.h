#ifndef BULLET_H
#define BULLET_H

#pragma once

#include <Box2D.h>
#include <SFML/Graphics.hpp>
#include "constants.h"
#include <list>

using namespace sf;

class Bullet
{
public:
    static std::list<Bullet*> ActiveBullets;
    static bool HaveDeadBullet;
    static void DisplayAll(RenderWindow&);

    //      xy,    world,  direction, damage, whose,   bullet speed
    Bullet(b2Vec2, b2World&, char, int, std::string, float = BULLET_SPEED);
    ~Bullet();

    void        display(RenderWindow&);
    void        setAsDead();
    int         getDamage();
    b2Body&     getBody();
    std::string getWhose();

private:
    int         damage_;
    std::string whose_;

    Texture*    bulletTexture_;
    Sprite*     bulletSprite_;
    b2Body*     bulletPhysicsBody_;
    bool        iDead_;

};

#endif // BULLET_H
