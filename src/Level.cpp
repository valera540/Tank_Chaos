#include "Level.h"
#include "Bullet.h"
#include "Physics.h"
#include <iostream>

Level::Level()
{
    window_ = new RenderWindow(VideoMode(1600, 1000), "Tanks Chaos", Style::Fullscreen);
    window_->setVerticalSyncEnabled(true);
    window_->setMouseCursorVisible(false);

    b2Vec2 gravity(0.0f, 0.0f);
    world_ = new b2World(gravity);
    world_->SetContactListener(new Physics());

    map_ = new Map(world_);

    hero_ = new Hero(15.0f, 14.0f, world_);
    Enemy::EnemiesFactory(*world_, enemiesList_);

    view_ = new View(window_->getDefaultView());

    hud_ = new HUD();
}

Level::~Level()
{
    Bullet::ActiveBullets.clear();
    delete map_;
    delete hero_;
    delete view_;
    delete hud_;
    delete window_;
}

bool Level::start()
{
    Clock clock;
    bool notRestart = true;
    while (true) {

        // Window close event
        Event event;
		while (window_->pollEvent(event))
			if (event.type == Event::Closed) {
				window_->close();
                return false;
			}

        if (!hero_->getLifeStatus() || !Enemy::getLifeStatus(enemiesList_)) {
            if (notRestart)
                clock.restart();
            notRestart = false;
            if (clock.getElapsedTime().asSeconds() > 0.1f) {
                if (!hero_->getLifeStatus())
                    restartFlag_ = showEndingScreen(*view_, *window_, false);
                else
                    restartFlag_ = showEndingScreen(*view_, *window_, true);
                break;
            }
        }

        // Clear window
        window_->clear(Color(255, 200, 107));
		
        // Drawing map
        map_->display(*window_);

        // Drawing hero, enemies and bullets
        hero_->display(*window_);
        Enemy::EnemiesDisplay(*window_, enemiesList_);
        Bullet::DisplayAll(*window_);

        // Set up camera
        window_->setView(*view_);

        if (Keyboard::isKeyPressed(Keyboard::Escape))
            return false;

        // Hero and enemies moving actions
        hero_->moving(*world_);
        Enemy::EnemiesMoving(*world_, enemiesList_);

        // Set up camera position
        view_->setCenter(hero_->getPosition().x, hero_->getPosition().y);

        // Draw HUD
        hud_->display(*window_, hero_->getHeroInfo(), *view_);

        // Display all drawed entities
		window_->display();

        // Set up physics accuracy
        world_->Step(1/60.0f, 8, 3);
	}
    return restartFlag_;
}

bool Level::showEndingScreen(View& view, RenderWindow& window, bool win)
{
    Texture messTexture;
    Sprite  messSprite;

    if (win) {
        messTexture.loadFromFile(PATH + "win.png");
        messTexture.setSmooth(true);
        messSprite.setTexture(messTexture);
        messSprite.setPosition(view.getCenter().x - 300.0f, view.getCenter().y - 150.0f);
    }
    else {
        messTexture.loadFromFile(PATH + "lose.png");
        messTexture.setSmooth(true);
        messSprite.setTexture(messTexture);
        messSprite.setPosition(view.getCenter().x - 400.0f, view.getCenter().y - 300.0f);
    }

    Texture restPanelTexture;
    Sprite restPanelSprite;

    restPanelTexture.loadFromFile(PATH + "RestartPanel.png");
    restPanelTexture.setSmooth(true);
    restPanelSprite.setTexture(restPanelTexture);
    restPanelSprite.setPosition(view.getCenter().x - 250.0f, view.getCenter().y + 100.0f + 100.0f);

    Text yes;
    Text no;

    Font messFont;
    messFont.loadFromFile(PATH + "fixedsys.ttf");

    yes.setFont(messFont);
    yes.setString("Yes - [SPACE]");
    yes.setCharacterSize(60);
    yes.setPosition(view.getCenter().x - 215.0f, view.getCenter().y + 260.0f);

    no.setFont(messFont);
    no.setString("No - [ESC]");
    no.setCharacterSize(60);
    no.setPosition(view.getCenter().x - 150.0f, view.getCenter().y + 320.0f);

    yes.setFillColor(Color::Black);
    no.setFillColor(Color::Black);

    while (true) {

        Event event;
        while (window_->pollEvent(event))
            if (event.type == Event::Closed) {
                window_->close();
                return false;
            }

        if (Keyboard::isKeyPressed(Keyboard::Space)) {
            window.close();
            return true;
        }
        if (Keyboard::isKeyPressed(Keyboard::Escape)) {
            window.close();
            return false;
        }

        window.draw(messSprite);
        window.draw(restPanelSprite);
        window.draw(yes);
        window.draw(no);

        window.display();
    }
}
