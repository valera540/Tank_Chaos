#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Hero.h"

using namespace sf;

class HUD
{
public:
    HUD();
	~HUD();

    void display(RenderWindow&, HeroInfo&, View&);
    void setMiniMap(RenderWindow&, View&);

private:
	Font font_;

    Text THealth_,
         TDamage_,
         TXY_,
         TVelocity_;

    String SHealth_,
           SDamage_,
           SXY_,
           SVelocity_;
};
