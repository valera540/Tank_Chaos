#include "Hero.h"
#include <cmath>
#include "constants.h"
#include "Physics.h"

Hero::Hero(float x, float y, b2World* world)
{
    maxHealth_ = 10;
	currentHealth_ = maxHealth_;
	damage_ = 3;
    nameId_ = "hero";
    speed_ = 5.0f;
    direction_ = 'N';
    iDead_ = false;
    rateOfFire_ = seconds(1.0f);

    x_ = x * 64.0f;
    y_ = y * 64.0f;

	entityTexture_ = new Texture();
    if (!entityTexture_->loadFromFile(PATH + "hero.png"))
		std::cout << "|  ERROR   | HERO| hero.png texture open error!" << '\n';

    /*###### PHYSICS BLOCK ######*/
    b2BodyDef bodyDef;
    bodyDef.position.Set(x_ * TO_METERS, y_ * TO_METERS);
    bodyDef.type = b2_dynamicBody;
    entityPhysicsBody_ = world->CreateBody(&bodyDef);

    b2PolygonShape shape;
    shape.SetAsBox(54.0f/2.0f * TO_METERS, 55.0f/2.0f * TO_METERS);

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.3f;

    entityPhysicsBody_->CreateFixture(&fixtureDef);
    entityPhysicsBody_->SetUserData(this);
    entityPhysicsBody_->SetFixedRotation(true);
    /*###########################*/

    entitySprite_.setTexture(*entityTexture_);
    entitySprite_.setPosition(entityPhysicsBody_->GetPosition().x*TO_PIXELS, entityPhysicsBody_->GetPosition().y*TO_PIXELS);
    entitySprite_.setOrigin(54.0f/2.0f, 55.0f/2.0f);

    heroInfo_.currHealth = std::to_string(currentHealth_);
    heroInfo_.maxHealth = std::to_string(maxHealth_);
    heroInfo_.damage = std::to_string(damage_);
    heroInfo_.x = std::to_string((int) x_);
    heroInfo_.y = std::to_string((int)y_);
}

Hero::~Hero()
{
	delete entityTexture_;
    entityPhysicsBody_->GetWorld()->DestroyBody(entityPhysicsBody_);
}

void Hero::moving(b2World& world)
{
    b2Vec2 vel = entityPhysicsBody_->GetLinearVelocity();

    updateHeroInfo(vel);

    /* ########## WASD CONTROL ########### */
    if (Keyboard::isKeyPressed(Keyboard::W)) {
        entityPhysicsBody_->SetLinearVelocity(b2Vec2(0.0f, -speed_));

        entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 0.0f );
        direction_ = 'N';
    }
    else if (Keyboard::isKeyPressed(Keyboard::S)) {
        entityPhysicsBody_->SetLinearVelocity(b2Vec2(0.0f, speed_));

        entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 180.0f / DEG);
        direction_ = 'S';
    }
    else if (Keyboard::isKeyPressed(Keyboard::D)) {
        entityPhysicsBody_->SetLinearVelocity(b2Vec2(speed_, 0.0f));

        entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 90.0f / DEG);
        direction_ = 'E';
    }
    else if (Keyboard::isKeyPressed(Keyboard::A)) {
        entityPhysicsBody_->SetLinearVelocity(b2Vec2(-speed_, 0.0f));

        entityPhysicsBody_->SetTransform(entityPhysicsBody_->GetWorldCenter(), 270.0f / DEG);
        direction_ = 'W';
    }
    else {
        /* ########## REVERCE FORCE ########## */
        if (vel.x != 0.0f && vel.x > -INERT_LIMITER && vel.x < INERT_LIMITER)
            entityPhysicsBody_->SetLinearVelocity(b2Vec2(0.0f, vel.y));
        else if (vel.x != 0.0f)
            entityPhysicsBody_->ApplyForceToCenter(b2Vec2(vel.x > 0.0f ? -REV_FORCE : REV_FORCE, 0.0f), 0);

        if (vel.y != 0.0f && vel.y > -INERT_LIMITER && vel.y < INERT_LIMITER)
            entityPhysicsBody_->SetLinearVelocity(b2Vec2(vel.x, 0.0f));
        else if (vel.y != 0.0f)
            entityPhysicsBody_->ApplyForceToCenter(b2Vec2(0.0f, vel.y > 0.0f ? -REV_FORCE : REV_FORCE), 0);
        /* ################################### */
    }

    entitySprite_.setRotation(entityPhysicsBody_->GetAngle() * DEG);
    /* ################################### */

    /* #### ATTACK ###### */
    if (Keyboard::isKeyPressed(Keyboard::Space)) {
        if (clockOfFire_.getElapsedTime() > rateOfFire_) {
            attack(world);
            clockOfFire_.restart();
        }
    }
    /* ################# */

    x_ = entityPhysicsBody_->GetPosition().x * TO_PIXELS;
    y_ = entityPhysicsBody_->GetPosition().y * TO_PIXELS;
    entitySprite_.setPosition(x_, y_);
}

HeroInfo& Hero::getHeroInfo()
{
    return heroInfo_;
}

void Hero::updateHeroInfo(const b2Vec2 &vec)
{
    heroInfo_.currHealth = std::to_string(currentHealth_);
    heroInfo_.maxHealth = std::to_string(maxHealth_);
    heroInfo_.damage = std::to_string(damage_);
    heroInfo_.vel_x = std::to_string((float) vec.x);
    heroInfo_.vel_y = std::to_string((float) vec.y);
    heroInfo_.y = std::to_string((int) y_);
    heroInfo_.x = std::to_string((int) x_);
}

void Hero::attack(b2World& world)
{
    Bullet::ActiveBullets.push_back(new Bullet(b2Vec2(x_, y_), world, direction_, damage_, "hero"));
}

void Hero::takeDamage(int damage)
{
	currentHealth_ -= damage;

    if (currentHealth_ <= 0) {
        iDead_ = true;
    }
}

void Hero::display(RenderWindow& window)
{
    window.draw(entitySprite_);
}

b2Vec2 Hero::getPosition()
{
    return b2Vec2(entityPhysicsBody_->GetPosition().x*TO_PIXELS, entityPhysicsBody_->GetPosition().y*TO_PIXELS);
}

bool Hero::getLifeStatus()
{
    return !iDead_;
}
